//
//  RequestLogsTableViewCell.swift
//  ApiHelper
//
//  Created by Ramses on 07/09/2017.
//  Copyright © 2017 BeMobile. All rights reserved.
//

import UIKit

protocol RequestLogsRowViewProtocol {
    
    func requestLogsRowViewProtocolNeedToShowComplete( request: String)
}

class RequestLogsRowView: UIView {
    
    var delegate: RequestLogsRowViewProtocol?
    
    private var requestComplete: String = ""
    
    private static let maxViewHeight: CGFloat = 2000
    private static let separatorHeight: CGFloat = 10
    private static let completeRequestButtonHeight: CGFloat = 50
    
    public func configCellandSetText(_ requestString: String){
        self.requestComplete = requestString
        
        
        let viewHeightAndOverflow = RequestLogsRowView.sizeHeightForViewWithText(requestString)
        let viewHeight = viewHeightAndOverflow.height
        let overFlow   = viewHeightAndOverflow.overflow
        
        
        let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: viewHeight))
        backgroundView.backgroundColor = UIColor.white
        self.addSubview(backgroundView)
        
        
        let textArea = UITextView(frame: CGRect(x: 0, y: 0,
                                                width: self.frame.width,
                                                height: viewHeight))
        textArea.font = UIFont.systemFont(ofSize: 10)
        textArea.backgroundColor = .clear
        textArea.text = requestString
        textArea.textColor = .black
        textArea.isScrollEnabled = false
        
        backgroundView.addSubview(textArea)
        
        
        let bottomDivLine = UIView(frame: CGRect(x: 0,
                                                 y:backgroundView.frame.size.height - RequestLogsRowView.separatorHeight,
                                                 width: self.frame.width,
                                                 height: RequestLogsRowView.separatorHeight))
        bottomDivLine.backgroundColor = UIColor.gray
        backgroundView.addSubview(bottomDivLine)
        
        
        if (overFlow == true){
            let yButton = backgroundView.frame.size.height - bottomDivLine.frame.size.height - RequestLogsRowView.completeRequestButtonHeight
            let completeRequestButton = UIButton(frame: CGRect(x: 0,
                                                               y: yButton,
                                                               width: self.frame.width,
                                                               height: RequestLogsRowView.completeRequestButtonHeight))
            
            completeRequestButton.backgroundColor = .blue
            completeRequestButton.setTitle("See complete request", for: .normal)
            completeRequestButton.setTitleColor(.white, for: .normal)
            completeRequestButton.addTarget(self, action: #selector(goToDetailButtonClicked(sender:)), for: .touchUpInside)
            self.addSubview(completeRequestButton)
        }
        
        
    }
    
    
    
    
    func goToDetailButtonClicked(sender: UIButton!){
        delegate!.requestLogsRowViewProtocolNeedToShowComplete(request: requestComplete)
    }
    
    
    
    
    
    
    
    
    private static func sizeHeightForText(_ requestString: String) -> CGFloat{
        let labelSizeHeight = Utils.dynamicHeight(text: requestString,
                                                  font: UIFont.systemFont(ofSize: 10),
                                                  width: UIScreen.main.bounds.size.width)
        
        return labelSizeHeight + 20 //gap por si se pela
    }
    
    public static func sizeHeightForViewWithText(_ requestString: String) -> (height: CGFloat, overflow: Bool){
        
        let labelSize    = self.sizeHeightForText(requestString)
        let completeSize = labelSize + separatorHeight
        
        if (completeSize > maxViewHeight){
            let height = maxViewHeight + completeRequestButtonHeight
            return (height: height, overflow: true)
        }else{
            return (height: completeSize, overflow: false)
        }
    
    }

    
    
    
    
    

}
