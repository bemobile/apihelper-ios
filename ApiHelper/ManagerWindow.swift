//
//  ManagerWindow.swift
//  Pods
//
//  Created by Ramses on 26/09/2017.
//
//

import Foundation


protocol ManagerWindowDelegate: class {
    func isPointEvent(point: CGPoint) -> Bool
}


class ManagerWindow: UIWindow {
    
    weak var delegate: ManagerWindowDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        self.windowLevel = UIWindowLevelStatusBar - 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        
        return false
        
        //return self.delegate?.isPointEvent(point: point) ?? false
    }
}
