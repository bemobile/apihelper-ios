//
//  ApiHelperNetworkLogsPluginForMoya.swift
//  BusUp
//
//  Created by Ramses on 16/08/2017.
//  Copyright © 2017 Bemobile. All rights reserved.
//

import Foundation
import Moya
import Result

class NetworkLogsPluginForMoya: PluginType{
    
    public func didReceive(_ result: Result<Moya.Response, MoyaError>, target: TargetType) {
        
        var requestUrlBase     = ""
        var requestUrlPath     = ""
        var requestMethod      = ""
        var requestHeaders     = ""
        var requestString      = ""
        var responseString     = ""
        var responseCodeString = ""
        
        
        if let response = result.value{
            
            responseString = self.dataToJsonString(data: response.data)
            
            if (response.request!.httpBody != nil){
                requestString  = self.dataToJsonString(data: response.request!.httpBody!)
            }
            
            if (response.request!.allHTTPHeaderFields != nil ){
                requestHeaders = self.dictToJsonString(dic: response.request!.allHTTPHeaderFields!)
            }
            
            requestMethod  = response.request!.httpMethod!
            requestUrlBase = response.request!.url!.host!
            requestUrlPath = response.request!.url!.path
            responseCodeString = String(response.statusCode)
            
        }
        
        ApiHelper.addToLogs(baseUrl: requestUrlBase, path: requestUrlPath, method: requestMethod, headers: requestHeaders, request: requestString, response: responseString, code: responseCodeString)
    }
    
    
    private func dataToJsonString(data: Data) -> String{
        var stringData = ""
        do {
            let dataAsJSON = try JSONSerialization.jsonObject(with: data)
            let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
            stringData = String(data: prettyData, encoding: String.Encoding.utf8) as String!
        } catch {
            stringData = "No response data!"
        }
        
        return stringData
    }
    
    
    private func dictToJsonString(dic: [String: String]?) -> String{
        var stringDict: String? = ""
        
        if (dic != nil){
            if let theJSONData = try? JSONSerialization.data( withJSONObject: dic!, options: [.prettyPrinted]) {
                
                stringDict = String(data: theJSONData, encoding: .utf8)
                print("JSON string = \(stringDict!)")
            }
            
            if (stringDict == nil){
                stringDict = "No request headers"
            }
        }
        
        return stringDict!
    }

    
}




