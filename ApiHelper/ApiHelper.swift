//
//  ApiHelper.swift
//  PatiPami
//
//  Created by Ramses on 14/08/2017.
//  Copyright © 2017 BeMobile. All rights reserved.
//

import Foundation
import UIKit
import Moya


/*
 
 Hacer los cambios 
 
 
 open -a Xcode ApiHelper.podspec
 
 Subir cambios a repo normal y hacer tag de la version
 
 pod repo push BemobilePodSpecs ApiHelper.podspec

 
 
 Pasoss 
 
 - Configurar el ApiHelperNetworkLogsPluginForMoya en moya
 - Confirar el push token 
 - Buscar lugar desde donde abrir el view pricipal
 
 
 
 */

/*
 
 Lista de pendientes.


 - Cambiar libreria a GitHub
 - agregar version de la app
 - Mostrar lista de ambientes (URLS)
 - Opcion para autorellenar formulario
 
 
 - Para la parte de ambientes hacer un constructor que se le pasa el ambieten al y lo acepte al menos de que  que se haya cmabiado expreseamente 

 
 #https://github.com/remirobert/Dotzu
 #https://github.com/Flipboard/FLEX
 
 
 */



public enum ApiHelperEnviroment{
    
    case mock
    case dev
    case preprod
    case prod
    
}


public class ApiHelper: NSObject{
    
    //private
    public final class PrivateLogsSingleton {
        
        private init() {
            
            URLProtocol.registerClass(TestClass.self)
            
        }
        
        static let shared = PrivateLogsSingleton()
        
        var pushToken: String = ""
        
        var apiEnviroment: ApiHelperEnviroment = .dev
    
        
    
        var window: ManagerWindow?
    }
    
    
    
    public static func startWithFloatingBall(){
    
        self.PrivateLogsSingleton.shared.window = ManagerWindow(frame: UIScreen.main.bounds)
        self.PrivateLogsSingleton.shared.window!.rootViewController = FloatingBallViewController()
        self.PrivateLogsSingleton.shared.window!.makeKeyAndVisible()
        
        //self.PrivateLogsSingleton.shared.window!.delegate = self
        
    }
    
    
    
    
    
    
    
    public static func openApiHelper(){
        
    
        if (UIApplication.shared.delegate != nil){
            let delegate = UIApplication.shared.delegate!
            
            if (delegate.window != nil && delegate.window! != nil){
                let window = delegate.window!!
                
                
                var topViewController = window.rootViewController!
                
                while( topViewController.presentedViewController != nil){
                    topViewController = topViewController.presentedViewController!
                }
                
                
                let navigation = UINavigationController(rootViewController: HomeViewController())
                navigation.navigationBar.backgroundColor = .gray
                
                navigation.modalPresentationStyle = .overFullScreen
                topViewController.present(navigation, animated: true, completion: nil)
            }
            
        }
        
    }
    
    
    
    
    public static func addToLogs(baseUrl: String, path: String, method: String, headers: String, request: String, response: String, code: String){
        let record = Record(baseUrl: baseUrl, path: path, method: method, headers: headers, request: request, response: response, code: code)
        let recordString = record.getStringRecord()
        Repository.addNewRecord(recordString)
    }
    
    @objc public static func addPushToken(_ token: String){
        PrivateLogsSingleton.shared.pushToken = token
    }
    
    
    public static func setNewEnviroment(_ env: ApiHelperEnviroment){
        PrivateLogsSingleton.shared.apiEnviroment = env
    }
    
    
    
    
    
    
    
    
    // Getters
    
    // Buscar manera de copiar los objetos 
    
    public static func getNetworkLogs() -> [String]{
        return Repository.getRecords()
    }
    
    public static func getPushToken() -> String{
        return "" + PrivateLogsSingleton.shared.pushToken
    }
    
    public static func getEnviroment() -> ApiHelperEnviroment{
        return PrivateLogsSingleton.shared.apiEnviroment
    }
    
    
    
    
    
    
    public static func getNetworkLoggerPluginForMoya() -> PluginType {
        return NetworkLogsPluginForMoya()
    }
}




extension ApiHelper: ManagerWindowDelegate {
    func isPointEvent(point: CGPoint) -> Bool {
        
        let viewController = PrivateLogsSingleton.shared.window!.rootViewController as! FloatingBallViewController
        return viewController.shouldReceive(point: point)
    }
}








open class TestClass: URLProtocol {
    
    open override class func canInit(with request: URLRequest) -> Bool {
        print("Request #\(1): URL = \(request.url?.absoluteString)")    
        return false
    }
    
    public func urlProtocol(_ protocol: URLProtocol, didReceive response: URLResponse, cacheStoragePolicy policy: URLCache.StoragePolicy) {
        
        print("Request #- 1 Response #\(1): URL = \(response.url?.absoluteString)")
        
    }
    
    
    func connection(_ connection: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        
        print("Request #- 2 Response #\(1): URL = \(response.url?.absoluteString)")
        
    }
    
    
}












