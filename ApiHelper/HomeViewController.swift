//
//  ApiHelperHomeViewController.swift
//  BusUp
//
//  Created by Ramses on 14/08/2017.
//  Copyright © 2017 Bemobile. All rights reserved.
//


import UIKit

class HomeViewController: UIViewController {
    
    private var envButtonsArray: [UIButton] = []
    private let envArray: [ApiHelperEnviroment] = [.mock, .dev, .preprod, .prod]
    private var pushTokenText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pushTokenText = ApiHelper.getPushToken()
        if (pushTokenText == ""){
            pushTokenText = "No tiene udid"
        }
        
        
        self.configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    // Private 
    
    func configureView(){
        
        self.title = "Api Helper Home"
        self.view.backgroundColor = .white
        
        let statusBarBackground = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        statusBarBackground.backgroundColor = .gray
        self.view.addSubview(statusBarBackground)
        
        
        // APP INFORMATION
        let appInfoTextView = UITextView()
        appInfoTextView.translatesAutoresizingMaskIntoConstraints = false
        appInfoTextView.text = appInformationText()
        appInfoTextView.textColor = .black
        appInfoTextView.textAlignment = .left
        appInfoTextView.isEditable = false
        
        appInfoTextView.backgroundColor = .red
        
        self.view.addSubview(appInfoTextView)
        
        self.view.addConstraint(NSLayoutConstraint(item: appInfoTextView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: appInfoTextView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: appInfoTextView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100))
        self.view.addConstraint(NSLayoutConstraint(item: appInfoTextView, attribute: .top, relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 30))
        
        
        
        // PUSH TOKEN
        let pushTokenButton = UIButton()
        pushTokenButton.translatesAutoresizingMaskIntoConstraints = false
        pushTokenButton.setTitle("Push Token - (Click to copy)\n \(pushTokenText)", for: .normal)
        pushTokenButton.addTarget(self, action: #selector(pushTokenClicked), for: .touchUpInside)
        pushTokenButton.setTitleColor(.black, for: .normal)
        pushTokenButton.titleLabel?.lineBreakMode = .byWordWrapping
        pushTokenButton.titleLabel?.numberOfLines = 3
        pushTokenButton.titleLabel?.textAlignment = .center
        pushTokenButton.layer.borderColor = UIColor.black.cgColor
        pushTokenButton.layer.borderWidth = 1
        self.view.addSubview(pushTokenButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: pushTokenButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: pushTokenButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: pushTokenButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80))
        self.view.addConstraint(NSLayoutConstraint(item: pushTokenButton, attribute: .top, relatedBy: .equal, toItem: appInfoTextView, attribute: .bottom, multiplier: 1, constant: 10))
        
        
        
        // NETWORK LOGS
        let logsButton = UIButton()
        logsButton.translatesAutoresizingMaskIntoConstraints = false
        logsButton.setTitle("Network Logs", for: .normal)
        logsButton.addTarget(self, action: #selector(networkLogsClicked), for: .touchUpInside)
        logsButton.setTitleColor(.blue, for: .normal)
        logsButton.layer.borderColor = UIColor.blue.cgColor
        logsButton.layer.borderWidth = 1
        self.view.addSubview(logsButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: logsButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: logsButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: logsButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50))
        self.view.addConstraint(NSLayoutConstraint(item: logsButton, attribute: .top, relatedBy: .equal, toItem: pushTokenButton, attribute: .bottom, multiplier: 1, constant: 10))
        

        // ENVIROMENTS
        let envLabel = UILabel()
        envLabel.translatesAutoresizingMaskIntoConstraints = false
        envLabel.text = "Api Enviroment (Sin integrar)"
        envLabel.textColor = .black
        envLabel.textAlignment = .center
        self.view.addSubview(envLabel)
        
        self.view.addConstraint(NSLayoutConstraint(item: envLabel, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: envLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: envLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30))
        self.view.addConstraint(NSLayoutConstraint(item: envLabel, attribute: .top, relatedBy: .equal, toItem: logsButton, attribute: .bottom, multiplier: 1, constant: 30))
        
        addEnviromentsAfertView(envLabel)
        
        
        // CLOSE
        let closeButton = UIButton()
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.setTitle("Close", for: .normal)
        closeButton.addTarget(self, action: #selector(closeClicked), for: .touchUpInside)
        closeButton.setTitleColor(.red, for: .normal)
        closeButton.layer.borderColor = UIColor.red.cgColor
        closeButton.layer.borderWidth = 1
        self.view.addSubview(closeButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0))
        
        
    }

    
    
    
    
    func networkLogsClicked(sender: UIButton!){
        let viewController = RequestLogsViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func closeClicked(sender: UIButton!){
        self.dismiss(animated: true, completion: nil)
    }
    
    func pushTokenClicked(sender: UIButton){
        UIPasteboard.general.string = pushTokenText
    }
    
    func envButtonClicked(sender: UIButton){
        
        for button in envButtonsArray{
            button.backgroundColor = UIColor(red: 180.0 / 255.0, green: 180.0 / 255.0, blue: 180.0 / 255.0, alpha: 1)
        }
        
        ApiHelper.setNewEnviroment(envArray[sender.tag])
        sender.backgroundColor = .gray
    }
    
    
    
    
    
    
    
    
    
    func addEnviromentsAfertView(_ view: UIView){
        
        
        for (index, env) in envArray.enumerated(){
            
            var title = ""
            switch env {
            case .mock: title    = "Mock"; break
            case .dev: title     = "Dev"; break
            case .preprod: title = "Preprod"; break
            case .prod: title    = "Prod"; break
            }
            
            
            let envButton = UIButton()
            envButton.translatesAutoresizingMaskIntoConstraints = false
            envButton.setTitle(title, for: .normal)
            envButton.tag = index
            envButton.addTarget(self, action: #selector(envButtonClicked), for: .touchUpInside)
            envButton.setTitleColor(.black, for: .normal)
            
            if (env == ApiHelper.getEnviroment()){
                envButton.backgroundColor = .gray
            }else{
                envButton.backgroundColor = UIColor(red: 180.0 / 255.0, green: 180.0 / 255.0, blue: 180.0 / 255.0, alpha: 1)
            }
            
            self.envButtonsArray.append(envButton)
            self.view.addSubview(envButton)
            
            self.view.addConstraint(NSLayoutConstraint(item: envButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: envButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: envButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50))
            self.view.addConstraint(NSLayoutConstraint(item: envButton, attribute: .top, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: CGFloat(10 + (index * 50)) ))
        
        }
    }
    
    func appInformationText() -> String{
        var info = ""
        info += "App Name : \(Bundle.main.infoDictionary?["CFBundleName"] as! String) \n"
        info += "Bundle Id : \(Bundle.main.bundleIdentifier!) \n"
        info += "Version Number : \(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String ) \n"
        info += "Build Number : \(Bundle.main.infoDictionary?["CFBundleVersion"] as! String ) \n"
        
        return info
    }
    
    
}
