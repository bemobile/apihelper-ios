//
//  ApiHelperRequestLogsViewController.swift
//  BusUp
//
//  Created by Ramses on 16/08/2017.
//  Copyright © 2017 Bemobile. All rights reserved.
//

import UIKit

class RequestLogsViewController: UIViewController {
    
    
    var scrollView: UIScrollView!
    var containerView = UIView()
    var containerViewHeightConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
        self.showInfo()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // Private
    
    private func configureView(){
        
        self.title = "Network Logs"
        self.view.backgroundColor = .white
        
        let statusBarBackground = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        statusBarBackground.backgroundColor = .gray
        self.view.addSubview(statusBarBackground)
        
        
        scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(scrollView)
        
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .top, relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0))
    
        
        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(containerView)
        
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .leading, relatedBy: .equal, toItem: self.scrollView, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .trailing, relatedBy: .equal, toItem: self.scrollView, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .top, relatedBy: .equal, toItem: self.scrollView, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .bottom, relatedBy: .equal, toItem: self.scrollView, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .centerX, relatedBy: .equal, toItem: self.scrollView, attribute: .centerX, multiplier: 1, constant: 0))
        self.containerViewHeightConstraint = NSLayoutConstraint(item: containerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        self.view.addConstraint(self.containerViewHeightConstraint!)
        
    }
    
    private func showInfo(){
        
        var yPosition:CGFloat = 0
        
        for (requestText) in ApiHelper.getNetworkLogs().reversed(){
            
            let rowHeight = RequestLogsRowView.sizeHeightForViewWithText(requestText).height
            
            let rect = CGRect(x: 0, y: yPosition, width: self.view.frame.width, height: rowHeight)
            print("rect of \(rect)")
            let rowView = RequestLogsRowView(frame: rect)
            rowView.delegate = self
            rowView.configCellandSetText(requestText)
            self.containerView.addSubview(rowView)
            
            yPosition = yPosition + rowHeight
        }
    
        print("\(yPosition)")
        self.containerViewHeightConstraint!.constant = yPosition
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    
}


extension RequestLogsViewController: RequestLogsRowViewProtocol{
    
    func requestLogsRowViewProtocolNeedToShowComplete(request: String) {
        let requestDetailViewController = RequestLogDetailViewController()
        requestDetailViewController.requestComplete = request
        self.present(requestDetailViewController, animated: true, completion: nil)
    }
    
}



