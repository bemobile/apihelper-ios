//
//  Utils.swift
//  ApiHelper
//
//  Created by Ramses on 07/09/2017.
//  Copyright © 2017 BeMobile. All rights reserved.
//

import Foundation


public class Utils {


    public static func dynamicHeight(text:String, font:UIFont, width:CGFloat) -> CGFloat{
    
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0,  width: width, height:  CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    

}
