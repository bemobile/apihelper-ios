//
//  Request.swift
//  ApiHelper
//
//  Created by Ramses on 07/09/2017.
//  Copyright © 2017 BeMobile. All rights reserved.
//

import Foundation


public class Record{
    
    
    private let baseUrl: String
    private let path: String
    private let method: String
    private let headers: String
    private let request: String
    private let response: String
    private let code: String
    
    init(baseUrl: String, path: String, method: String, headers: String, request: String, response: String, code: String) {
        self.baseUrl  = baseUrl
        self.path     = path
        self.method   = method
        self.headers  = headers
        self.request  = request
        self.response = response
        self.code     = code
    }
    
    
    func getStringRecord()-> String{
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date = dateFormatter.string(from: Date())
        
        var rows: [String] = []
        // Falta ponernle un poco mas de formato y algun sperador mejor
        
        rows.append("TIME: \(date)")
        rows.append("URL: \(self.baseUrl)")
        rows.append("PATH: \(self.path)")
        rows.append("REQUEST TYPE: \(self.method)")
        rows.append("REQUEST HEADERS: \n\(self.headers)")
        rows.append("REQUEST DATA: \n\(self.request)")
        rows.append("RESPONSE CODE: \(self.code)")
        rows.append("RESPONSE DATA: \n\(self.response)")
        let logRecord = String(rows.joined(separator: "\n"))
        
        
        return logRecord!
    }
    
    
}
