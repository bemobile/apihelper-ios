//
//  RequestLogDetailViewController.swift
//  ApiHelper
//
//  Created by Ramses on 07/09/2017.
//  Copyright © 2017 BeMobile. All rights reserved.
//

import UIKit

class RequestLogDetailViewController: UIViewController {

    var requestComplete: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        self.title = "Network Logs Detail"
        self.view.backgroundColor = .white
        
        let statusBarBackground = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        statusBarBackground.backgroundColor = .gray
        self.view.addSubview(statusBarBackground)
        
    
        
        let textArea = UITextView()
        textArea.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(textArea)
        
        self.view.addConstraint(NSLayoutConstraint(item: textArea, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: textArea, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: textArea, attribute: .top, relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: textArea, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0))
        
        textArea.text = requestComplete
    
        
        
        
        let buttonTest = UIButton(frame: CGRect(x: (self.view.frame.size.width - 150),
                                                y: 20,
                                                width: 150,
                                                height: 50))
        buttonTest.backgroundColor = .gray
        buttonTest.setTitle("Close", for: .normal)
        buttonTest.setTitleColor(.black, for: .normal)
        buttonTest.setTitleColor(.gray, for: .highlighted)
        buttonTest.addTarget(self, action: #selector(closeScreenButtonClicked(_:)), for:.touchUpInside)
        self.view.addSubview(buttonTest)
    }
    
    
    func closeScreenButtonClicked(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }


}
