//
//  Repository.swift
//  ApiHelper
//
//  Created by Ramses on 07/09/2017.
//  Copyright © 2017 BeMobile. All rights reserved.
//

import Foundation


public class Repository{
    
    static public func addNewRecord(_ recordString: String){
    
        var records = UserDefaults.standard.array(forKey: Constants.Keys.recordsKey) as? [String] ?? [String]()
        records.append(recordString)
        
        if (records.count > Constants.Global.maxRecords){
            let removeCount = records.count - Constants.Global.maxRecords
            records = Array(records.dropFirst(removeCount))
        }
        
        UserDefaults.standard.set(records, forKey: Constants.Keys.recordsKey)
    }
    
    static public func getRecords() -> [String]{
        return UserDefaults.standard.array(forKey: Constants.Keys.recordsKey) as? [String] ?? [String]()
    }
    

}
