//
//  Constants.swift
//  ApiHelper
//
//  Created by Ramses on 07/09/2017.
//  Copyright © 2017 BeMobile. All rights reserved.
//

import Foundation


struct Constants {

    
    struct Global {
        
        static let maxRecords   = 20
        
    }
    


    struct Keys {
        static let recordsKey = "apihelper_records_key"
    }
}
